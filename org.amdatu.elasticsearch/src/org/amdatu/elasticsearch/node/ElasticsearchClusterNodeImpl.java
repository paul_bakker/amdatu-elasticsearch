/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.elasticsearch.node;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.log.LogService;

public class ElasticsearchClusterNodeImpl implements Node {

	private volatile LogService logService;
	
	private final Settings settings;
	private Node node;

	public ElasticsearchClusterNodeImpl(Settings settings) throws ConfigurationException {
		this.settings = settings;
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void dmStart() {

		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(settings.getClassLoader());
			
			NodeBuilder nodeBuilder = NodeBuilder.nodeBuilder();
			
			nodeBuilder.settings(settings);
	
			node = nodeBuilder.node();
		}finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void dmStop() {
		try {
			node.close();
		}catch (Exception e){
			logService.log(LogService.LOG_WARNING, "Exception in node.close() ", e );
		}
	}

	public Client client() {
		return node.client();
	}

	public void close() {
		throw new RuntimeException("Node.close is not supported");
	}

	public boolean isClosed() {
		return node.isClosed();
	}

	public Settings settings() {
		return node.settings();
	}

	public Node start() {
		throw new RuntimeException("Node.start is not supported");
	}

	public Node stop() {
		throw new RuntimeException("Node.stop is not supported");
	}

	public String getClusterName() {
		return settings.get("cluster.name");
	}
	
}
